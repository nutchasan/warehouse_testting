from unittest.mock import MagicMock, patch
import unittest
import image


class TestsImage(unittest.TestCase):

    def assert_data(self, expected, result):
        self.assertEqual(expected, result)

    def not_assert_data(self, expected, result):
        self.assertNotEqual(expected, result)

    @patch('image.send_file')
    @patch('image.Response')
    @patch('image.models')
    def test_check_have_image_should_be_OK(self,
                                           models_mock,
                                           respond_mock,
                                           sendfile_mock):
        file_name = 'eiei.jpg'

        respond = MagicMock()
        sendfile = MagicMock()

        image_mock = MagicMock()
        image_mock.filename = 'eiei.jpg'

        user_mock = MagicMock()
        user_mock.image = image_mock

        models_mock.User.objects().first.return_value = user_mock
        respond_mock.return_value = respond
        sendfile_mock.return_value = sendfile

        result = image.user_image(file_name)
        expected = 404

        self.not_assert_data(expected, result.status_code)

    @patch('image.send_file')
    @patch('image.Response')
    @patch('image.models')
    def test_check_wrong_filename_should_be_404(self,
                                                models_mock,
                                                respond_mock,
                                                sendfile_mock):
        file_name = '555.jpg'

        respond = MagicMock()
        sendfile = MagicMock()

        image_mock = MagicMock()
        image_mock.filename = 'eiei.jpg'

        user_mock = MagicMock()
        user_mock.image = image_mock

        models_mock.User.objects().first.return_value = user_mock
        respond_mock.return_value = respond
        sendfile_mock.return_value = sendfile

        result = image.user_image(file_name)
        expected = 404

        self.assert_data(expected, result.status_code)

    @patch('image.send_file')
    @patch('image.Response')
    @patch('image.models')
    def test_check_none_filename_should_be_404(self,
                                               models_mock,
                                               respond_mock,
                                               sendfile_mock):
        file_name = 'eiei.jpg'

        respond = MagicMock()
        sendfile = MagicMock()

        image_mock = MagicMock()
        image_mock.filename = None

        user_mock = MagicMock()
        user_mock.image = image_mock

        models_mock.User.objects().first.return_value = user_mock
        respond_mock.return_value = respond
        sendfile_mock.return_value = sendfile

        result = image.user_image(file_name)
        expected = 404

        self.assert_data(expected, result.status_code)


if __name__ == "__main__":
    unittest.main()
