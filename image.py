from flask import Response, send_file
import models


def user_image(file_name):
    user = models.User.objects().first()
    img = None
    if user.image.filename == file_name:
        img = user.image
    response = Response()
    if img:
        response = send_file(img, mimetype='image/jpg')
    else:
        response.status_code = 404

    return response
