import models
import datetime


def get_right_dateFormat(offset: int = 0) -> str:
    return str((datetime.datetime.now() -
                datetime.timedelta(days=offset)).strftime("%Y-%m-%d"))


def fetch_token():
    token = models.OAuthToken.objects().first()
    return token
