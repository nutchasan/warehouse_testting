from unittest.mock import MagicMock, patch
import unittest
import datetime
from datetime import timedelta
import oauth


class TestsOauth(unittest.TestCase):

    def assert_data(self, expected, result):
        self.assertEqual(expected, result)

    def not_assert_data(self, expected, result):
        self.assertNotEqual(expected, result)

    def test_check_today_date_should_be_equal(self):
        expected = datetime.datetime.now().strftime("%Y-%m-%d")
        result = oauth.get_right_dateFormat()
        self.assert_data(expected, result)

    def test_check_yesterday_date_should_be_equal(self):
        num = 1
        date = datetime.datetime.now() - timedelta(days=1)
        expected = date.strftime("%Y-%m-%d")
        result = oauth.get_right_dateFormat(num)
        self.assert_data(expected, result)

    @patch('oauth.models')
    def test_check_empty_oauth_should_be_none(self, models_mock):
        expected = None
        models_mock.OAuthToken.objects().first.return_value = None
        result = oauth.fetch_token()
        self.assert_data(expected, result)

    @patch('oauth.models')
    def test_check_oauth_should_be_equal(self, models_mock):
        oauth_mock = MagicMock()
        models_mock.OAuthToken.objects().first.return_value = [oauth_mock]
        expected = oauth.fetch_token()
        result = oauth.fetch_token()
        self.assert_data(expected, result)

    @patch('oauth.models')
    def test_check_oauth_with_access_key_123456_should_be_equal(self,
                                                                models_mock):
        oauth_mock = MagicMock()
        oauth_mock.access = '123456'
        models_mock.OAuthToken.objects().first.return_value = [oauth_mock]
        result = oauth.fetch_token()
        expected = '123456'
        for i in result:
            self.assert_data(expected, i.access)

    @patch('oauth.models')
    def test_check_oauth_access_key_123123_should_be_notequal(self,
                                                              models_mock):
        oauth_mock = MagicMock()
        oauth_mock.access = '123456'
        models_mock.OAuthToken.objects().first.return_value = [oauth_mock]
        result = oauth.fetch_token()
        expected = '123123'
        for i in result:
            self.not_assert_data(expected, i.access)


if __name__ == "__main__":
    unittest.main()
