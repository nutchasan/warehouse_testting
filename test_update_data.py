from unittest.mock import MagicMock, patch
import unittest
import datetime
import update_data


class TestsUpdateData(unittest.TestCase):
    def assert_data(self, expected, result):
        self.assertEqual(expected, result)

    def not_assert_data(self, expected, result):
        self.assertNotEqual(expected, result)

    @patch('update_data.models')
    def test_check_empty_user_should_be_none(self, models_mock):
        expected = None
        models_mock.User.objects().first.return_value = None
        result = update_data.update_data()

        self.assert_data(expected, result)

    @patch('update_data.models')
    def test_check_user_should_be_equal(self, models_mock):
        user_mock = MagicMock()
        models_mock.User.objects().first.return_value = user_mock
        result = update_data.update_data()
        expected = user_mock
        self.assert_data(expected, result)

    @patch('update_data.models')
    def test_check_user_cal_limit_should_be_2000(self, models_mock):
        user_mock = MagicMock()
        models_mock.User.objects().first.return_value = user_mock
        result = update_data.update_data()
        expected = 2000
        self.assert_data(expected, result.cal_limit)

    @patch('update_data.models')
    def test_check_user_cal_limit_per_week_should_be_14000(self, models_mock):
        user_mock = MagicMock()
        models_mock.User.objects().first.return_value = user_mock
        result = update_data.update_data()
        expected = 14000
        self.assert_data(expected, result.cal_limit_per_week)

    @patch('update_data.models')
    def test_check_user_cal_limit_over_week_should_be_Fasle(self, models_mock):
        user_mock = MagicMock()
        models_mock.User.objects().first.return_value = user_mock
        result = update_data.update_data()
        expected = False
        self.assert_data(expected, result.cal_limit_over)

    @patch('update_data.models')
    def test_check_user_end_week_date_should_be_equal(self, models_mock):
        user_mock = MagicMock()
        models_mock.User.objects().first.return_value = user_mock
        result = update_data.update_data()
        expected = datetime.datetime.now() + datetime.timedelta(days=7)
        self.assert_data(expected.strftime("%Y-%m-%d"),
                         result.end_week_date.strftime("%Y-%m-%d"))

    @patch('update_data.models')
    def test_check_user_calories_in_should_be_0(self, models_mock):
        user_mock = MagicMock()
        models_mock.User.objects().first.return_value = user_mock
        result = update_data.update_data()
        expected = 0
        self.assert_data(expected, result.calories_in)


if __name__ == "__main__":
    unittest.main()
