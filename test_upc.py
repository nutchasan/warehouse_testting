import unittest
import upc_p


class TestsUPC(unittest.TestCase):

    def assert_data(self, expected, result):
        self.assertEqual(expected, result)

    def not_assert_data(self, expected, result):
        self.assertNotEqual(expected, result)

    def test_check_no_upc_should_be_NOT_OK(self):
        upc = ''
        expected = 'OK'
        item = upc_p.UPCItemDBClient()
        result = item.lookup(upc)
        self.not_assert_data(expected, result['code'])

    def test_check_wrong_upc_should_be_NOT_OK(self):
        upc = '12345678999'
        expected = 'OK'
        item = upc_p.UPCItemDBClient()
        result = item.lookup(upc)
        self.not_assert_data(expected, result['code'])

    def test_check_upc_less_than_12_should_be_NOT_OK(self):
        upc = '123456'
        expected = 'OK'
        item = upc_p.UPCItemDBClient()
        result = item.lookup(upc)
        self.not_assert_data(expected, result['code'])

    def test_check_upc_more_than_12_should_be_NOT_OK(self):
        upc = '123456789123456789'
        expected = 'OK'
        item = upc_p.UPCItemDBClient()
        result = item.lookup(upc)
        self.not_assert_data(expected, result['code'])

    def test_check_upc_not_digit_should_be_NOT_OK(self):
        upc = 'abc!@#'
        expected = 'OK'

        item = upc_p.UPCItemDBClient()
        result = item.lookup(upc)

        self.not_assert_data(expected, result['code'])

    def test_check_bacon_upc_should_be_OK(self):
        upc = '015900060009'
        expected = 'OK'

        item = upc_p.UPCItemDBClient()
        result = item.lookup(upc)

        self.assert_data(expected, result['code'])


if __name__ == "__main__":
    unittest.main()
