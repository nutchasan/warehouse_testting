import models
import datetime


def update_data():
    user = models.User.objects().first()
    if not user:
        return None
    user.cal_limit = 2000
    user.cal_limit_per_week = 14000
    user.cal_limit_over = False
    user.end_week_date = datetime.datetime.now() + datetime.timedelta(days=7)
    user.calories_in = 0
    return user
